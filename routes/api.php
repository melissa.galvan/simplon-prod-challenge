<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// List all promos
Route::resource('promos', 'PromoController'); // Route to show all promos

Route::resource('students', 'StudentController'); // Route to show all students

Route::resource('student/create', 'StudentController'); // Route to create a student

// List a single post
// Route::resource('promo/{id}', 'PromoController');
